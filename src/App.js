import React from 'react';
import AddProduct from "./components/AddProduct";
import ProductLister from "./components/productLister";
import './app.scss';


const products = [
  {
    name: "hero Product",
    detail: "Lorem ipsum dolor sit amet",
    price: "99",
    hero: "OMG This just came out today!",
    image: "http://placehold.it/300x300/999/CCC"
  },
  {
    name: "Product 1",
    detail: "Lorem ipsum dolor sit amet",
    price: "99",
    info: "This is the latest and greatest product from Derp corp.",
    image: "http://placehold.it/300x300/999/CCC"
  },
  {
    name: "Product 2",
    detail: "Lorem ipsum dolor sit amet",
    price: "99",
    offer: "BOGOF",
    image: "http://placehold.it/300x300/999/CCC"
  },
  {
    name: "Product 3",
    detail: "Lorem ipsum dolor sit amet",
    price: "99",
    image: "http://placehold.it/300x300/999/CCC"
  },
  {
    name: "Product 4",
    detail: "Lorem ipsum dolor sit amet",
    price: "99",
    offer: "No srsly GTFO",
    image: "http://placehold.it/300x300/999/CCC"
  },
  {
    name: "Product 5",
    detail: "Lorem ipsum dolor sit amet",
    price: "99",
    image: "http://placehold.it/300x300/999/CCC"
  },
  {
    name: "Product 6",
    detail: "Lorem ipsum dolor sit amet",
    price: "99",
    info: "This is the latest and greatest product from Derp corp.",
    offer: "info with offer",
    image: "http://placehold.it/300x300/999/CCC"
  }
];

export default class App extends React.Component{
  render() {
    return (
        <div className="app d-flex flex-column w-100 h-100 justify-content-center align-items-center">
          <AddProduct/>
          <hr/>
          <ProductLister products={products}/>    {/* propu burdan otururem */}
        </div>
    );
  }
}

