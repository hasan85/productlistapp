import React, {Component} from 'react';

class AddProduct extends Component {
	constructor(props) {
		super(props);
		this.state = {
			name: "",
			price: "",
			rating: "",
			description: "",
			image: "",
		}
	}

	handleChange = e => this.setState({
		[e.target.name]: e.target.value     // bunu sene izah elemisdim
		// isin sirri one solution for all cases
	});

	handleSubmit = e => {
		e.preventDefault();
		console.log("current state -> ", this.state);
	};

	render() {
		return (
			<div>
				<h1>Add new Product!</h1>
				<form onSubmit={this.handleSubmit}>
					<div className="form-group">
						<label htmlFor="product-name">Product name</label>
						<input onChange={this.handleChange} name={"name"} type="text" className="form-control"
						       id="product-name"
						       placeholder="Product name"/>
					</div>
					<div className="form-group">
						<label htmlFor="product-price">Product price</label>
						<input onChange={this.handleChange} name={"price"} type="number" className="form-control"
						       id="product-price"
						       placeholder="Product price"/>
					</div>
					<div className="form-group">
						<label htmlFor="product-rating">Product rating</label>
						<select onChange={this.handleChange} name={"rating"} className="form-control"
						        id="product-rating">
							<option>1</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
							<option>5</option>
						</select>
					</div>
					<div className="form-group">
						<label htmlFor="product-image">Product image</label>
						<input name={"image"} type="file" alt="Submit"/>
					</div>
					<div className="form-group">
						<label htmlFor="product-description">Product description</label>
						<textarea onChange={this.handleChange} name={"description"} className="form-control"
						          id="product-description" rows="3"/>
					</div>
					<input type="submit" value={"ADD"}/>
				</form>
			</div>
		);
	}
}

export default AddProduct;
