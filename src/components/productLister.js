import React, {Component} from 'react';
import "./style.scss"

class ProductLister extends Component {
	constructor(props) {
		super(props);
		this.state = {
			products: props.products
		}
	}

//  Yeni dersimize xos gelmisiniz
	render() {
		return (
			<div className="d-flex flex-row justify-content-center align-items-center flex-wrap cards-holder">
				{this.state.products && this.state.products.map(
					(product, index) => {
						return(
							<div className="product-card col-3" key={index}>
								{/* map eliyende htmli root elemente yeni indiki halda div.product-card -a key prop
								u vermey lazimdi cunki iterasiya eliyende onnar qarisir nolur yeni de problem cixir
								deqiq neye lazimdi hec bilmirem esas odu yadinda saxla ki olmalidi ve unique olmalidi */}
								<img src={product.image} alt="" className="img-fluid"/>
								<p>{product.name}</p>
								<p>{product.detail}</p>
								<p>price: {product.price}</p>
							</div>
						)
					}
				)}
			</div>
		);
	}
}

export default ProductLister;
